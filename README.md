# README #

A "first principles"/ground up attempt at getting a stable AT commands implementation working with an Arduino Uno + Nano, using the ESP8266 chip, to connect to the Thingspeak API using the POST HTTP method.

### What is this repository for? ###

I've provided for review by participants at the OzBerryPi IoT meetup group for review.

See the companion class-based library, which is a first attempt at refactoring this (semi-)working code into a library format.  The code is generally cleaner in that version, and the comments are more complete.

### How do I get set up? ###

Download the .ino file and open in the Arduino IDE (I have built this using v1.6.5 of the IDE).

Note that I have enabled compiling with C++11, so some syntax that I'm using may require this flag to be enabled.  [Details of how to enable C++11](http://stackoverflow.com/questions/16224746/how-to-use-c11-to-program-the-arduino).  On Mac OS X:

1. Find app in Applications folder
2. Ctrl click choose “Show package contents"
3. Navigate to and open Contents > Java > hardware > arduino > avr > platform.txt
4. Find the line starting with "compiler.cpp.flags="
5. Add "-std=gnu++11" at the end

You will need to have an ESP8266 chip of some description connected to the digital pins 8 (Arduino Receive-->ESP Transmit) and 9 (Arduino Transmit-->ESP Receive) using a logic level converter (or equivalent) for 5v-based Arduinos.

### Contribution guidelines ###

This code is provided for illustrative purposes only.

If you have notes/suggestions/would like to connect, please contact Grant Young via http://zum.io/contact/